package message.controller;

import message.domian.Message;
import message.service.IMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;

@Controller
public class MessageController {

    @Autowired
    private IMessageService messageService;

    @MessageMapping("/private")
    public void message(Message message) throws Exception {
        messageService.newMessage(message);
    }

}