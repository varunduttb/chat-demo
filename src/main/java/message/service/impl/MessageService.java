package message.service.impl;

import message.domian.Message;
import message.model.MessageType;
import message.service.IMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@Service
public class MessageService implements IMessageService  {

    @Autowired
    private SimpMessagingTemplate messagingTemplete;

    @Override
    public void newMessage(Message message) {
        String destination;
        if (message.getMessageType().equals(MessageType.USER)) {
            destination = "/messages/private/" + message.getTo();
        } else {
            destination = "/messages/group/" + message.getTo();
        }
        messagingTemplete.convertAndSend(destination, message);
    }
}
