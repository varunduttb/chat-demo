var stompClient = null;
var username = "Varun.Dutt";

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#privateLogs").show();
        //$("#publicLogs").show();
    }
    else {
        $("#privateLogs").hide();
        //$("#publicLogs").hide();
    }
    $("#privateLogBody").html("");
    //$("#publicLogBody").html("");
}

function connect() {
    var socket = new SockJS('http://localhost:8080/messages');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        //listenGroup();
        username = $("#currentUser").val();
        console.log("Username", username);
        listenPrivate(username);
    });
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function sendName() {
    //$("#privateLogs > tbody").html("");
    var username = $("#name").val();
    //listenPrivate(username);
    var to = $("#to").val();
    var messageText = $("#message").val();
    var message = {
            'from': username,
            'to': to,
            'message': messageText,
            'messageType': "USER"
        }
    stompClient.send("/messages/private", {}, JSON.stringify(message));
}

/*function listenGroup() {
    stompClient.subscribe('/messages/'+ client +'/group', function (progress) {
                showGroup(progress);
    });
}*/

function listenPrivate(username) {
    stompClient.subscribe('/messages/private/' + username, function (progress) {
                showPrivate(progress);
    });
}

function showPrivate(progress) {
    console.log("Message", progress);
    $("#privateLogBody").append("<tr><td>" + progress.body + "</td></tr>");
}

/*
function showGroup(progress) {
    console.log("Message", progress);
    $("#publicLogBody").append("<tr><td>" + progress.body + "</td></tr>");
}
*/

$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#connect" ).click(function() { connect(); });
    $( "#disconnect" ).click(function() { disconnect(); });
    $( "#send" ).click(function() { sendName(); });
});
